﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class HealthBarC : MonoBehaviour
{
    [SerializeField]

    private int maxHealth;
    private int health;
    public event Action<float> OnHealthPctChanged = delegate { };

    // Start is called before the first frame update
    void Start()
    {
        maxHealth = 10;
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {


    }

    void DealDamage(int damage)
    {
        health -= damage;
        float currentHealthPct = health / maxHealth;
        OnHealthPctChanged(currentHealthPct);
        if (health <= 0) ;
        {
            Die();
        }
    }

    void FixedUpdate()
    {

    }

    void Die()
    {
        Destroy(this);
    }

    void OnCollisionEnter(Collision collide)
    {
        if (collide.gameObject.name == "Arrow(Clone)")
        {
            DealDamage(1);

        }

        if (collide.gameObject.name == "Spear")
        {
            DealDamage(2);



        }
    }

}
