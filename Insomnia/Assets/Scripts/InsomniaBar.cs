﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InsomniaBar : MonoBehaviour
{

    public Image foregroundImage;
    private float updateSpeedSeconds = 0.5f;
    public int maxHealth;
    private int health;
    ////////  public event Action<float> OnHealthPctChanged = delegate { };
    public int timeFor1PctDecrease = 3;
    public int decreaseMultiplier = 3;

    // Start is called before the first frame update
    void Start()
    {
        //     GetComponentInParent<HealthBar>().OnHealthPctChanged += HandleHealthChanged;

        maxHealth = 100;
        health = maxHealth;
        StartCoroutine("LoseSleep");

        timeFor1PctDecrease = 10;

    }

    public void HandleHealthChanged(float pct)
    {
        StartCoroutine(ChangeToPct(pct));

    }

    public IEnumerator ChangeToPct(float pct)
    {
        float preChangePct = foregroundImage.fillAmount;
        float elapsed = 0f;
        float changePct = preChangePct - (pct / 100);
        while (elapsed < updateSpeedSeconds)
        {
            elapsed += Time.deltaTime;
            foregroundImage.fillAmount = Mathf.Lerp(preChangePct, changePct, elapsed / updateSpeedSeconds);
          //  print(pct + "Tijd" + Time.deltaTime);
            yield return null;
        }

        //  foregroundImage.fillAmount = pct * 100;

    }

    void Update()
    {

    }



    private IEnumerator LoseSleep()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeFor1PctDecrease);
            Debug.Log("dealDMG " + decreaseMultiplier);
            StartCoroutine(ChangeToPct(decreaseMultiplier * 1));
            //    DealDamage(decreaseMultiplier * 1);

        }

    }

    void DealDamage(int damage)
    {
        health -= damage;
        Debug.Log("Health " + health);
        Debug.Log("Dmg " + damage);
        float currentHealthPct = health / maxHealth;
        StartCoroutine(ChangeToPct(currentHealthPct));
        if (health <= 0)
        {
            Debug.Log("die?? " + health);
            Die();

        }
    }

   public void Heal(int HealAmount)
    {
        health += HealAmount;
        Debug.Log("Health " + health);
      //  Debug.Log("Dmg " + damage);
        float currentHealthPct = health / maxHealth;
        StartCoroutine(ChangeToPct(currentHealthPct));
    }

    void Die()
    {
        Destroy(this);
    }

    void OnCollisionEnter(Collision collide)
    {
        /*
        if (collide.gameObject.name == "Arrow(Clone)")
        {
            DealDamage(1);
          
        }

        if (collide.gameObject.name == "Spear")
        {
            DealDamage(2);
        


        }

    */
    }

}

