﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class InsomniahBar : MonoBehaviour
{
    [SerializeField]

    public int maxHealth;
    private int health;
    public event Action<float> OnHealthPctChanged = delegate { };
    public int timeFor1PctDecrease;
    public int decreaseMultiplier = 1; 
    // Start is called before the first frame update
    void Start()
    {
        maxHealth = 100;
        health = maxHealth;
        StartCoroutine("LoseSleep");

        timeFor1PctDecrease = 60;
    }

    // Update is called once per frame
    void Update()
    {


    }

    IEnumerator LoseSleep()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeFor1PctDecrease);
            DealDamage(decreaseMultiplier * 1);

        }

    }
    void DealDamage(int damage)
    {
        health -= damage;
        float currentHealthPct = health / maxHealth;
        OnHealthPctChanged(currentHealthPct);
        if (health <= 0) ;
        {
            Die();
        }
    }

    void FixedUpdate()
    {

    }

    void Die()
    {
        Destroy(this);
    }

    void OnCollisionEnter(Collision collide)
    {
        /*
        if (collide.gameObject.name == "Arrow(Clone)")
        {
            DealDamage(1);
          
        }

        if (collide.gameObject.name == "Spear")
        {
            DealDamage(2);
        


        }

    */
    }

}
