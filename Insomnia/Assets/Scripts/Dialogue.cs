﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    public Text textDisplay;
    public Text nameDisplay;
    public string charName;
    public string[] sentences;
    private int index;
    public float typingSpeed = 0.02f;

    void Update()
    {
        nameDisplay.color = Color.white;
        if (textDisplay.text == sentences[index] )
        {
            StartCoroutine(NextSentence());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(Type());
        }
    }

    public IEnumerator Type()
    {

        //Zorgt voor het verschijnen van text per letter
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.color = Color.white;
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public IEnumerator NextSentence()
    {
        if (index< sentences.Length - 1)
        {
            index++;

            yield return new WaitForSeconds(3);
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            yield return new WaitForSeconds(3);
            textDisplay.text = "";
        }
    }
}
