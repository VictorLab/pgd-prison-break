﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
public class CountDown : MonoBehaviour
{
    public int hoursLeft = 0; //Seconds Overall
   public  int daysLeft = 3;
    public Text countdown; //UI Text Object
    public int timeFor1Hr;
    void Start()
    {
        StartCoroutine("LoseTime");
        Time.timeScale = 1; //set timeScale to original (unnecesary)
    }
    void Update()
    {
        if (daysLeft > -1)
        {

            countdown.text = ("Days: " + daysLeft  + " Hrs: "  +  hoursLeft); //Showingtimme
        }
    }
    
    //Simple Coroutine
    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeFor1Hr);
            if (hoursLeft >  0)
            {
                hoursLeft--; //-uur
            }
            else if (hoursLeft < 1) {
                daysLeft--; //-dag
              
                if (daysLeft < 0)
                {
                    countdown.text = ("Game Over"); //Showing game over 

                }

                hoursLeft = 24;
            }

           /* if (timeLeft < -3)
            {
                Application.LoadLevel(0);
            } */
        }
        

    } 
}