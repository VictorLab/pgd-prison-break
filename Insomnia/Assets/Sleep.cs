﻿using UnityEngine;

public class Sleep : MonoBehaviour
{
	public GameObject Player;
	private PlayerStats playerStats;

	void Start(){
		playerStats = Player.GetComponent<PlayerStats>();
	}

	void OnMouseDown (){
		if(playerStats.stamina <= 75){
			playerStats.stamina += 15;
			playerStats.drowsiness += 10;
		}else{
			Debug.Log("You're slept too much");
		}

		if(playerStats.drowsiness >= 100){
			playerStats.drowsiness = 100;
		}

		if(playerStats.stamina >= 100){
			playerStats.stamina = 100;
		}
		Debug.Log(playerStats.drowsiness);
		Debug.Log(playerStats.stamina);
	}
}
