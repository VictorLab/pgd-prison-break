﻿using UnityEngine;

public class workOut : MonoBehaviour
{
	public GameObject Player;
	private PlayerStats playerStats;

	void Start(){
		playerStats = Player.GetComponent<PlayerStats>();
	}

	void OnMouseDown (){
		if(playerStats.stamina >= 0){
		playerStats.strength += 2;
		playerStats.stamina -= 20;
		playerStats.drowsiness -= 5;
		} else {	
			
			Debug.Log("You're too tired");
		}

		if(playerStats.stamina == 0 || playerStats.stamina <= 0){
			playerStats.stamina = 0;
		}


		Debug.Log(playerStats.stamina);
		Debug.Log(playerStats.strength);
	}
}
