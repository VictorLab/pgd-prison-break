﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
	public int intelligence = 5;
	public int social = 5;
	public int strength = 5;
	public int stamina = 100;
	public int drowsiness = 100;

	public void Check (){
		if(drowsiness >= 100){
			drowsiness = 100;
			Debug.Log(drowsiness);
		}
	}
}



